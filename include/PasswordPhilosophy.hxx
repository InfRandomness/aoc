#ifndef AOC_PASSWORDPHILOSOPHY_HXX
#define AOC_PASSWORDPHILOSOPHY_HXX

#include "Day.hxx"

class PasswordPhilosophy final : public Day {
  public:
    PasswordPhilosophy() = default;

    void run(std::span<std::string_view> args) override;
};

#endif // AOC_PASSWORDPHILOSOPHY_HXX
