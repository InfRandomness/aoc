#ifndef AOC_DAY_HXX
#define AOC_DAY_HXX

#include <memory>
#include <string>
#include <vector>
#include <span>

class Day {
  public:
    virtual ~Day();
    virtual void run(std::span<std::string_view> args) = 0;
};

auto getDay(std::string_view day) -> std::unique_ptr<Day>;

#endif // AOC_DAY_HXX