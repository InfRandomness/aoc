#ifndef AOC_STRING_HXX
#define AOC_STRING_HXX

#include <string>
#include <vector>

namespace String {
    /// Converts a char** into an std::vector<std::string> by spaces
    ///
    /// Usage:
    /// \code
    ///     char *str[] = { "bla", "bla", "bla", "bla" }
    ///     cstrArrayToVector(4, str);
    /// \endcode
    ///
    /// \param length the number of char * into the char **
    /// \param array the char ** containing the elements
    ///
    /// \returns vector<std::string> containing the arguments as std::string
    std::vector<std::string_view> cstrArrayToVector(int length, char **array);
}

#endif // AOC_STRING_HXX
