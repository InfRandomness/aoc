﻿# CMakeList.txt : CMake project for AoC, include source and define
# project specific logic here.

cmake_minimum_required(VERSION 3.8)

project("AoC")

file(GLOB SRC src/*.cxx src/days/*.cxx)

# Add entrypoint and commands to project's executable.
add_executable(AoC ${SRC})

# Set include/ as the include directory
target_include_directories(AoC PRIVATE include/)

# Set the project as a c++20 project
target_compile_features(AoC PUBLIC cxx_std_20)

# Enable exceptions if MSVC is used
if ("${CMAKE_CXX_SIMULATE_ID}" STREQUAL "MSVC")
    target_compile_options(AoC PUBLIC /EHs)
endif ()

if (CMAKE_BUILD_TYPE STREQUAL "Debug")
    set(CMAKE_CXX_FLAGS "-fsanitize=address -fno-optimize-sibling-calls -fsanitize-address-use-after-scope -fno-omit-frame-pointer -g -o1")

    set(CMAKE_CXX_FLAGS "-fsanitize=leak -fno-omit-frame-pointer -g -O1")

    set(CMAKE_CXX_FLAGS "-fsanitize=undefined")
endif ()

# TODO: Add tests and install targets if needed.