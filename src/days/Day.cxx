#include "ReportRepair.hxx"
#include "PasswordPhilosophy.hxx"

#include <memory>
#include <string>

Day::~Day() = default;

auto getDay(std::string_view day) -> std::unique_ptr<Day> {
    if (day == "day1") {
        return std::make_unique<ReportRepair>();
    }
    if (day == "day2") {
        return std::make_unique<PasswordPhilosophy>();
    }
    return {};
}