#include "ReportRepair.hxx"

void ReportRepair::run(std::span<std::string_view> args) {
    auto stream = IO::openFile(std::string(args[1]));

    std::vector<int> numbers = IO::readFileToIntVector(stream);

    auto result = partOne::calculateValue(numbers);
    std::cout << "The product of the part one is : " << result << "\n";

    result = partTwo::calculateValue(numbers);
    std::cout << "The product of the part two is : " << result << "\n";
}

int partOne::calculateValue(std::vector<int> numbers) {
    for (int i = 0; i != numbers.size(); i++) {
        for (int j = 0; j != numbers.size(); j++) {
            if (numbers.at(i) + numbers.at(j) == 2020) {
                return numbers.at(i) * numbers.at(j);
            }
        }
    }
}

int partTwo::calculateValue(std::vector<int> numbers) {
    for (int i = 0; i != numbers.size(); i++) {
        for (int j = 0; j != numbers.size(); j++) {
            for (int k = 0; k != numbers.size(); k++)
                if (numbers.at(i) + numbers.at(j) + numbers.at(k) == 2020) {
                    return numbers.at(i) * numbers.at(j) * numbers.at(k);
                }
        }
    }
}
