#include "IO.hxx"

#include <fstream>
#include <iostream>
#include <vector>

std::vector<int> IO::readFileToIntVector(std::ifstream &file) {
    std::vector<int> res;
    int i = 0;

    while (file >> i) {
        res.push_back(i);
    }

    return res;
}

std::ifstream IO::openFile(const std::string &path) {
    std::ifstream stream(path);

    if (!stream.is_open())
    {
        std::cerr << "An error has occurred while trying to open the file " << path << "\n";
        std::terminate();
    }

    return stream;
}