﻿#include "Day.hxx"
#include "String.hxx"

#include <span>

int main(int ac, char *av[]) {
    auto args = String::cstrArrayToVector(ac, av);
    std::span<std::string_view> span{args};
    auto day = getDay(span[1]);
    day->run(span.subspan(1));
    return 0;
}